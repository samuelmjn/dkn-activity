

		
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
                <h3 class="page-title">Tutup Buku</h3>  
					<div class="row">
						<div class="col-md-12">
							<!-- TODO LIST -->
							<div class="panel">
								<div class="panel-heading">
								<h3 class="panel-title">Disclaimer</h3>
								</div>
								<div class="panel-body">
                                <h5>1. Pastikan tanggal buku telah sesuai dengan tanggal server</h5>
                                <br><h5>2. Proses tutup buku tidak dapat direverse</h5>
                                <br><h5>3. Kegiatan pending yang tidak diotorisasi akan secara otomatis dianggap <b>EXPIRED</b> oleh sistem</h5>
                                <?php if ($this->session->userdata('closing') == false){?>
                                <br><div id="result">
                                <table id="table" class="table table-hover table-responsive" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nama Kegiatan</th>
                                                <th>Waktu Mulai</th>
                                                <th>Waktu Selesai</th>
                                                <th>Otorisasi</th>
                                            </tr>    
                                        </thead>
                                        <tbody>
                                            <?php $count = 0; ?>
                                            <?php foreach ($todo->result() as $r){?>
                                            <tr>
                                                <?php $count++ ?>
                                                <td><?php echo $count; ?></td>
                                                <td><?php echo $r->Activity ?></td>
                                                <td><?php echo $r->DateTime ?></td>
                                                <td><?php echo $r->DueDateTime ?></td>
                                                <td>
                                                    <form action="authorize" method="POST">
                                                    <input name="todoid" id="todoid" value="<?php echo $r->TodoID ?>" hidden></input>
                                                    <input name="excuse" id="excuse" class="form-control" placeholder="Alasan"></input>
                                                    <button class="btn btn-info">Otorisasi</button>
                                                    </form>
                                                </td>
                                            </tr> 
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    </div>
                                
                                    <?php if ($count >0) { ?>
                                    <br><a href="close" onclick="return  confirm('Apakah anda yakin untuk menutup buku? Semua kegiatan yang belum diotorisasi akan dianggap EXPIRED!')"><button class="btn btn-success">Tutup Buku</button></a>
                                   <?php  }else{ ?>
                                    <br><a href="close" onclick="return  confirm('Apakah anda yakin untuk menutup buku?')"><button class="btn btn-success">Tutup Buku</button></a>
                                   <?php  } ?>
                                   <?php }else{ ?>
                                   <br><h3><b>Anda telah melakukan tutup buku!</b></h3>
                                   <?php } ?>
								</div>
							</div>
						
						</div>
						
					</div>
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->

		