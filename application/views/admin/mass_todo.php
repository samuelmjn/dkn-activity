

		
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
                <h3 class="page-title">Tambah To-do Massal</h3>
					<div class="row">
						<div class="col-md-12">
							<!-- PANEL FORM -->
							<div class="panel panel-headline">
								<br>
								<div class="panel-body">
								<form method="POST" action="mass_todo_save">
								<label for="activity">Nama Aktivitas</label>
                                <input name="activity" id="activity" type="text" class="form-control" placeholder="Nama Kegiatan" required>
								<br>
								<label for="description">Keterangan</label>
                                <textarea name="description" id="description" class="form-control" placeholder="Keterangan" rows="4" required></textarea>
								<br>
								<label for="category">Kategori</label>
                                    <select name="category" id="category" class="form-control" required>
                                        <option selected disabled>Kategori</option>
                                        <?php foreach($category->result() as $c) { ?>
                                            <option value="<?php echo $c->CategoryName; ?>"><?php echo $c->CategoryName; ?></option>
                                        <?php } ?>
                                    </select>
									<br>
									<?php if ($this->session->userdata('closing') == true){
									$tomorrowObj = new DateTime('tomorrow');
									$tomorrow = $tomorrowObj->format('Y-m-d');
									?>
									<label for="datetime">Waktu Mulai Kegiatan</label>
									<input name="datetime" id="datetime" type="datetime-local" class="form-control" placeholder="Waktu Kegiatan" min="<?php echo $tomorrow . 'T00:00:00';?>" required>
									<br>
									<label for="duedatetime">Waktu Selesai Kegiatan</label>
                                    <input name="duedatetime" id="duedatetime" type="datetime-local" class="form-control" placeholder="Waktu Tenggang Kegiatan" min="<?php echo $tomorrow . 'T00:00:00';?>" required>
                                    <br>
									<?php }else{
									$today = date("Y-m-d"); 
									?>
									<label for="datetime">Waktu Mulai Kegiatan</label>
									<input name="datetime" id="datetime" type="datetime-local" class="form-control" placeholder="Waktu Kegiatan" min="<?php echo $today . 'T00:00:00';?>" required>
									<br>
									<label for="duedatetime">Waktu Selesai Kegiatan</label>
                                    <input name="duedatetime" id="duedatetime" type="datetime-local" class="form-control" placeholder="Waktu Tenggang Kegiatan" min="<?php echo $today . 'T00:00:00';?>" required>
                                    <br>
									<?php } ?>
                                    <label for="usercategory">Kepada</label><br>
                                    <select name="usercategory[]" style="width:100%" id="usercategory[]" class="form-control chosen-select" multiple required>
                                     
                                        <?php foreach($usercategory->result() as $u) { ?>
                                            <option value="<?php echo $u->UserCategory; ?>"><?php echo $u->UserCategory; ?></option>
                                        <?php } ?>
                                    </select>
									<br><br>
									<button class="btn btn-primary" type="submit">Simpan</button>
									<br>
								</form>
									
								</div>
							</div>
							<!-- END PANEL FORM -->
						</div>
						
					</div>
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->

