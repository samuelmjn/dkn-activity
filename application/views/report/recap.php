

		
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
                <h3 class="page-title">Rekapitulasi Kegiatan</h3>  
					<div class="row">
						<div class="col-md-12">
							<!-- TODO LIST -->
							<div class="panel">
							<!--	<div class="panel-heading">
									<h3 class="panel-title">To-Do List</h3> 
									
								</div> -->
								<div class="panel-body">
								    <form method="POST" action="recap">
                                        <label for="name">Nama Karyawan</label>
                                        <br><select style="width:250px" name="name" id="name" class="form-control chosen-select" required>
                                            <?php foreach($user->result() as $u) { ?>
                                            <option value="<?php echo $u->UserID ?>"><?php echo $u->Name ?></option>
                                            <?php } ?>
                                        </select><br>
                                        <br><button name="search" id="search" class="btn btn-primary">Cari</button>
                                    </form>
                                    <?php if (isset($result)) { ?>
                                    <br><div id="result">
                                    <table id="table" class="table table-striped table-hover" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nama Kegiatan</th>
                                                <th>Waktu Mulai</th>
                                                <th>Waktu Selesai</th>
                                                <th>Status</th>
                                            </tr>    
                                        </thead>
                                        <tbody>
                                            <?php foreach ($result->result() as $r){?>
                                            <tr>
                                                <td><?php echo $r->UserID ?></td>
                                                <td><?php echo $r->Activity ?></td>
                                                <td><?php echo $r->DateTime ?></td>
                                                <td><?php echo $r->DueDateTime ?></td>
                                                <td><?php echo $r->Status ?></td>
                                            </tr> 
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                            </div> <?php } ?>
								</div>
							</div>
							<!-- END TODO LIST -->
						</div>
						
					</div>
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->

		