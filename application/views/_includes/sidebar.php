<body>
	<!-- WRAPPER -->
	<div id="wrapper">

<!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="index.html"><img src="<?php echo base_url(); ?>assets/img/logo-dark.png" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<form class="navbar-form navbar-left">
					<div class="input-group">
						<input type="text" value="" class="form-control" placeholder="Search dashboard...">
						<span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
					</div>
				</form>
				
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
								<i class="lnr lnr-alarm"></i>
								<span class="badge bg-danger">5</span>
							</a>
						<ul class="dropdown-menu notifications">
								<li><a href="#" class="notification-item"><span class="dot bg-warning"></span>System space is almost full</a></li>
								<li><a href="#" class="notification-item"><span class="dot bg-danger"></span>You have 9 unfinished tasks</a></li>
								<li><a href="#" class="notification-item"><span class="dot bg-success"></span>Monthly report is available</a></li>
								<li><a href="#" class="notification-item"><span class="dot bg-warning"></span>Weekly meeting in 1 hour</a></li>
								<li><a href="#" class="notification-item"><span class="dot bg-success"></span>Your request has been approved</a></li>
								<li><a href="#" class="more">See all notifications</a></li>
							</ul>
						</li>
					
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo base_url(); ?>assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $this->session->userdata("name"); ?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
								<li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
								<li><a href="<?php echo base_url(); ?>Login/logout"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
						
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="<?php echo base_url(); ?>Dashboard"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
						<li><a href="<?php echo base_url(); ?>Todo" ><i class="lnr lnr-code"></i> <span>To-Do List</span></a></li>
						<li><a href="<?php echo base_url(); ?>Schedule" class=""><i class="lnr lnr-chart-bars"></i> <span>Schedule</span></a></li>
						<?php if ($this->session->userdata('usercategory') == "Supervisor") { ?>
						<li>
							<a href="#Admin" data-toggle="collapse" class="collapsed"><i class="lnr lnr-cog"></i> <span>Administratif</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="Admin" class="collapse ">
								<ul class="nav">
									<li><a href="<?php echo base_url(); ?>Admin/mass_todo" class="">Tambah To-do Massal</a></li>
									<li><a href="" class="">Tambah Pengumuman</a></li>
									<li><a href="<?php echo base_url(); ?>Admin/closing" class="">Tutup Buku</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#Report" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Laporan</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="Report" class="collapse ">
								<ul class="nav">
									<li><a href="<?php echo base_url(); ?>Report/recap" class="">Rekapitulasi Kegiatan</a></li>
									<li><a href="" class="">Realisasi Target</a></li>
								</ul>
							</div>
						</li>
						<?php } ?>
						<li><a href="<?php echo base_url(); ?>Login/logout" class=""><i class="lnr lnr-linearicons"></i> <span>Log Out</span></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->