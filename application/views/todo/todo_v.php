

		
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
                <h3 class="page-title">Lihat Aktivitas</h3>
					<div class="row">
						<div class="col-md-12">
                            <!-- PANEL FORM -->
							<div class="panel panel-headline">
								<br>
								<div class="panel-body">
                                <?php foreach($todo->result() as $t) { ?>
								<form method="POST" action="<?php echo base_url(); ?>Todo/verification" enctype="multipart/form-data">
								<input name="todoid" id="todoid" value="<?php echo $t->TodoID; ?>" hidden>
								<label for="activity">Nama Aktivitas</label>
                                <input name="activity" id="activity" type="text" class="form-control" value="<?php echo $t->Activity; ?>" disabled>
								<br>
								<label for="description">Keterangan</label>
                                <textarea name="description" id="description" class="form-control" disabled><?php echo $t->Description; ?></textarea>
								<br>
								<label for="category">Kategori</label>
                                    <select name="category" id="category" class="form-control" disabled>
                                        <option selected disabled><?php echo $t->Category; ?></option>
                                    </select>
									<br>
									<label for="datetime">Waktu Mulai Kegiatan</label>
									<input name="datetime" id="datetime" value="<?php echo $t->DateTime; ?>" class="form-control" disabled>
									<br>
									<label for="duedatetime">Waktu Selesai Kegiatan</label>
                                    <input name="duedatetime" id="duedatetime" class="form-control" value="<?php echo $t->DueDateTime; ?>" disabled>
									<br>
									<?php if ($t->Action <> ''){ ?>
									<label for="action">Aksi</label><br>

									<?php 
									if ($t->Action == 'Call') { ?>
										<h5><a href="tel:<?php echo $t->ActionTo;?>">Telepon Nasabah</a></h5>
							
									<?php }elseif ($t->Action == 'SMS') { ?>
										<h5><a href="sms:<?php echo $t->ActionTo;?>">SMS Nasabah</a></h5>
							
									<?php }elseif ($t->Action == 'Survey') { ?>
										<a href="https://maps.google.com/?q=<?php echo $t->ActionTo;?>">Kunjungi Nasabah</a>
										<h5>(<?php echo $t->ActionTo; ?>)</h5>

									<?php }elseif ($t->Action == 'Email') { ?>
										<h5><a href="mailto:<?php echo $t->ActionTo;?>">Email Nasabah</a></h5>
								
									<?php }elseif ($t->Action == 'SP') { ?>
										<h5>Kirim Surat Peringatan</h5>
							
									<?php }elseif ($t->Action == 'Other') { ?>
										<h5><?php echo $t->ActionTo;?></h5>
									<?php } ?>

									<?php } ?>
									<br>
									<?php if ($t->Status == '1') {?>
									<label for="file">Upload Bukti Kegiatan</label>
									<input type="file" name="file" id="file" size="20" class="form-control" required>
									<br>
									<button class="btn btn-primary" type="submit">Kegiatan Selesai</button>
									<?php } ?>

									
									<br>
                                </form>
                                <?php } ?>
									
								</div>
							</div>
							<!-- END PANEL FORM -->
						</div>
						
					</div>
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
