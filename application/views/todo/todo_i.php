

		
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
                <h3 class="page-title">To-Do List</h3>  
					<div class="row">
						<div class="col-md-12">
							<!-- TODO LIST -->
							<div class="panel">
								<div class="panel-heading">
								<!--	<h3 class="panel-title">To-Do List</h3> -->
									
								</div>
								<div class="panel-body">
									<a href="Todo/add"><button class="btn btn-success">Tambah Kegiatan</button></a><br><br>
									<div id='calendar'></div>
								</div>
							</div>
							<!-- END TODO LIST -->
						</div>
						
					</div>
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->

		<!-- CALENDARJS -->
		<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/moment.min.js'></script>
<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/jquery.min.js'></script>
<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/jquery-ui.min.js'></script>
<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/fullcalendar.min.js'></script>
<script>


    // page is now ready, initialize the calendar...

    $('#calendar').fullCalendar({
			
    	   defaultView: 'listDay',
        // put your options and callbacks here
		views: {
      listDay: { buttonText: 'list day' },
      listWeek: { buttonText: 'list week' },
      listMonth: { buttonText: 'list month' }
    },

    header: {
      left: 'title',
      center: '',
      right: 'listDay,listWeek,listMonth'
    },

		events: [
<?php foreach($todo->result() as $t) { ?>
    {
    	title  : '<?php echo $t->Activity; ?>',
			url		 : 'Todo/view/<?php echo $t->TodoID; ?>',
	  	start  : '<?php echo $t->DateTime; ?>',
	  	end	 : '<?php echo $t->DueDateTime; ?>',
    },
  <?php } ?>
  ]
    });


</script>