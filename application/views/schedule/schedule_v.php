

		
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
                <h3 class="page-title">Schedule</h3>  
					<div class="row">
						<div class="col-md-12">
							<!-- TODO LIST -->
							<div class="panel">
								<div class="panel-heading">
								<!--	<h3 class="panel-title">To-Do List</h3> -->
									
								</div>
								<div class="panel-body">
									<div id="calendar"></div>
								</div>
							</div>
							<!-- END TODO LIST -->
						</div>
						
					</div>
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->

		<!-- CALENDARJS -->
<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/moment/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.1.1/fullcalendar.min.js"></script>
<script>
		$('#calendar').fullCalendar({
    themeSystem: 'bootstrap3',
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,listMonth'
    },
	weekNumbers: true,
	contentHeight: 'auto',
    eventLimit: true, // allow "more" link when too many events
	events: [
<?php foreach($todo->result() as $t) { ?>
    {
      title  : '<?php echo $t->Activity; ?>',
	  start  : '<?php echo $t->DateTime; ?>',
	  end	 : '<?php echo $t->DueDateTime; ?>',
    },
  <?php } ?>
  ]
  });
  </script>