<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends CI_Controller {

    function __construct(){
		parent::__construct();		
		if($this->session->userdata('status') != "1"){
			redirect(base_url("Login"));
		}
		$this->load->model('M_todo'); 
    }
    
	public function index()
	{
        $data['todo'] = $this->M_todo->index();
        $data['page'] = 'schedule/schedule_v';
		$this->load->view('template' , $data);
	}
}
