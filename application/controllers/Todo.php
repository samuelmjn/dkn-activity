<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todo extends CI_Controller {

	function __construct(){
		parent::__construct();		
		if($this->session->userdata('status') != "1"){
			redirect(base_url("Login"));
		}
		$this->load->model('M_todo'); 
		$this->load->model('M_category');
	}
	
	public function index()
	{
		$data['page'] = 'todo/todo_i';
		$data['todo'] = $this->M_todo->index();
 		$this->load->view('template' , $data);
	}
	
    
    public function add()
	{
		$data['page'] = 'todo/todo_a';
		$data['category'] = $this->M_category->index();
		$this->load->view('template' , $data);
	}

	public function view()
	{
		$id = $this->uri->segment('3');
		$data['page'] = 'todo/todo_v';
		$data['todo'] = $this->M_todo->view($id);
 		$this->load->view('template' , $data);
	}

	public function verification()
	{
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name'] = $this->input->post('todoid');
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload('file')) {
    	echo $this->upload->display_errors();
            
        } else {
            $data = array('image_metadata' => $this->upload->data());

            redirect ('/Todo');
        }
	}
	public function save()
	{
		$activity = $this->input->post('activity');
		$description = $this->input->post('description');
		$category = $this->input->post('category');
		$datetime = $this->input->post('datetime');
		$duedatetime = $this->input->post('duedatetime');
		$userid = $this->session->userdata('userid');
		$status = '2';

		$data = array(
			'Activity' => $activity,
			'Description' => $description,
			'Category' => $category,
			'DateTime' => $datetime,
			'UserID' => $userid,
			'DueDateTime' => $duedatetime,
			'Status' => $status
			);

			$this->M_todo->save($data);
			redirect('Todo');
	}
}
