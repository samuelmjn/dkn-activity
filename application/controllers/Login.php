<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
        parent::__construct();	
        $this->load->model('M_login'); 
        $this->load->model('M_closing');
    }
    
    public function index()
	{
        if($this->session->userdata('status') == "1"){
			redirect(base_url("Dashboard"));
		}	
 		$this->load->view('login');
    }

    public function logout()
    {
    $this->session->sess_destroy();
	redirect(base_url('login'));
    }
    
    public function login()
    {
        if($this->session->userdata('status') == "1"){
			redirect(base_url("Dashboard"));
		}	
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $credit = array(
            'Email' => $email,
            'Password' => md5($password)
        );
        $userdata = $this->M_login->login($credit);
        $closing = $this->M_closing->check_closing();
        $check = $this->M_login->login($credit)->num_rows();
       foreach($userdata->result() as $u) {
        $name = $u->Name;
        $usercategory = $u->UserCategory;
        $userid = $u->UserID;
        }
        if ($check > 0){
            $session = array(
                'email' => $email,
                'name' => $name,
                'usercategory' => $usercategory,
                'userid' => $userid,
                'status' => "1",
                'closing' => $closing
            );

        $this->session->set_userdata($session);
        redirect ('Dashboard');
        } else{
            redirect ('Login');
        }
    }
    
    
}