<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
		parent::__construct();	
		if($this->session->userdata('status') != "1"){
			redirect(base_url("Login"));
		}	
        $this->load->model('M_todo'); 
        $this->load->model('M_category');
		$this->load->model('M_usercategory');
		$this->load->model('M_login');
		$this->load->model('M_closing');
	}

	public function closing()
	{
        $data['todo'] = $this->M_closing->closing();
        $data['page'] = 'admin/closing';
        $this->load->view('template' , $data);
	}

	public function authorize(){
		$todoid = $this->input->post('todoid');
		$excuse = $this->input->post('excuse');

		$data = array(
			'Excuse' => $excuse,
			'Status' => '4'
		);

		$this->M_closing->authorize($todoid,$data);
		redirect('Admin/closing');
	}
    
    public function mass_todo_save()
	{
		$activity = $this->input->post('activity');
		$description = $this->input->post('description');
		$category = $this->input->post('category');
		$datetime = $this->input->post('datetime');
        $duedatetime = $this->input->post('duedatetime');
        $usercategory = $this->input->post('usercategory');
		$status = '1';

		foreach ($usercategory as $c) {
		$user = $this->M_login->find_user_category($c);
		
		foreach ($user->result() as $u) {
			$data = array(
				'Activity' => $activity,
				'Description' => $description,
				'Category' => $category,
				'DateTime' => $datetime,
				'DueDateTime' => $duedatetime,
				'UserCategory' => $c,
         	  	'UserID' => $u->UserID,
				'Status' => $status
				);

			$this->M_todo->save($data);
			}
		}
		redirect('Admin/mass_todo');
	}

	public function index()
	{
		redirect('Dashboard');
	}
	
	public function close()
	{
		$count = $this->M_closing->closing_count();
			if ($count > 0){
				$result = $this->M_closing->closing()->result();
				foreach ($result as $r){
					$data = array(
						'Status' => '5'
					);
			
					$this->M_closing->authorize($r->TodoID,$data);
				}
				$this->M_closing->add_closing();
				redirect('Dashboard');
			 } 
			
			else{
				$this->M_closing->add_closing();
				redirect('Dashboard');
			}

	}

    public function mass_todo()
    {
        $data['category'] = $this->M_category->index();
        $data['usercategory'] = $this->M_usercategory->index();
        $data['page'] = 'admin/mass_todo';
        $this->load->view('template' , $data);
    }
    
}