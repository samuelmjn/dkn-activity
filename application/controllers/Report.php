<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	function __construct(){
		parent::__construct();	
		if($this->session->userdata('status') != "1"){
			redirect(base_url("Login"));
		}	
        $this->load->model('M_todo'); 
        $this->load->model('M_login');
	}

	public function index()
	{
        $data['page'] = '';
		$this->load->view('template' , $data);
    }
    
    public function recap()
	{
        if (empty($this->input->post('name'))){
            $data['user'] = $this->M_login->index();
            $data['page'] = 'report/recap';
            $this->load->view('template' , $data); 
        } else{
            $data['user'] = $this->M_login->index();
            $userid = $this->input->post('name');
            $data['result'] = $this->M_todo->search($userid);
            $data['page'] = 'report/recap';
            $this->load->view('template', $data);
        }
	}
}
