<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_closing extends CI_Model
{
function closing()
    {
      $this->db->from('Todo');
      $this->db->where('Status', '1');
      $this->db->where('date(DueDateTime)', 'CURDATE()', FALSE);
      return $this->db->get();
    }

    function closing_count()
    {
      $this->db->from('Todo');
      $this->db->where('Status', '1');
      $this->db->where('date(DueDateTime)', 'CURDATE()', FALSE);
      return $this->db->count_all_results();
    }

    function authorize($id, $data){
        $this->db->where('TodoID' , $id);
        $this->db->update('Todo', $data);
    }

    function add_closing(){
        $date = date("Y-m-d");
        $data = array(
            'Date' => $date
        );
        $this->db->insert('Closing', $data);
    }

    function check_closing(){
        $this->db->from('Closing');
        $this->db->where('Date', 'CURDATE()', FALSE);
        $count = $this->db->count_all_results();
        if ($count > 0){
            $stat = true;
            return $stat;
        }
        else {
            $stat = false;
            return $stat;
        }
    }
}