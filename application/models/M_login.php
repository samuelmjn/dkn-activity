<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model
{
    function index(){
        return $this->db->get('UserAccount');
    }

    function login($credit)
    {
        return $this->db->get_where('UserAccount',$credit);
    }

    function find_user_category($category){
        $this->db->where('UserCategory',$category);
        $this->db->from('UserAccount');
        return $this->db->get();
    }
}

?>