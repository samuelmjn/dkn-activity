<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_todo extends CI_Model
{
    function index(){
    $userid = $this->session->userdata("userid");
    $this->db->from('Todo');
    $this->db->where('UserID', $userid);
		return $this->db->get();
    }

    function view($data){
      $this->db->from('Todo');
      $this->db->where('TodoID', $data);
      return $this->db->get();
    }

    function search($data){
      $this->db->from('Todo');
      $this->db->where('UserID', $data);
      return $this->db->get();
    }
    
    function save($data){
        $this->db->insert('Todo',$data);
    }

}

?>