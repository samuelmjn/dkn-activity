<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_loan extends CI_Model
{
    function index($id){
        $credit = $this->load->database('loan', TRUE);
        $credit->where('LoanID', $id);
        return $credit->get('Loan');
    }

}

?>